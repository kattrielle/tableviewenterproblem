package tableViewProblem.controls

import javafx.collections.ObservableList
import javafx.event.Event
import javafx.event.EventTarget
import javafx.scene.control.TableCell
import javafx.scene.control.TableView
import javafx.scene.input.KeyCode
import javafx.scene.input.KeyEvent
import tornadofx.*

// EventTarget.exceltableview
fun <T> EventTarget.exceltableview(items: ObservableList<T>? = null, op: TableView<T>.() -> Unit = {}) =
        TableView<T>().attachTo(this, op) {
            if (items != null) {
                if (items is SortedFilteredList<T>) items.bindTo(it)
                else it.items = items
            }
            it.enableExcelBehaviour()
        }

// TableView.enableExcelBehaviour
fun <T> TableView<T>.enableExcelBehaviour() {
    enableCellEditing()
    regainFocusAfterEdit()
    addEventHandler(KeyEvent.KEY_PRESSED) { keyEvent ->
        // For editing cell without pressing enter first
        if (keyEvent.code.isValidInput()) {
            if (editingCell == null) {
                val currentSelectedCell = selectedCell
                if (currentSelectedCell != null && currentSelectedCell.tableColumn.isEditable) {
                    edit(currentSelectedCell.row, currentSelectedCell.tableColumn)
                    //a trying to set KeyPressed to TextField
                    val focusedControl = scene?.focusOwner
                    Event.fireEvent( focusedControl, keyEvent.copyFor( keyEvent.source, focusedControl ) )
                }
            }
        }
    }
}

// KeyCode.isSymbol
fun KeyCode.isSymbol(): Boolean {
    return when (this) {
        KeyCode.BACK_QUOTE, KeyCode.MINUS, KeyCode.EQUALS,
        KeyCode.OPEN_BRACKET, KeyCode.CLOSE_BRACKET, KeyCode.BACK_SLASH,
        KeyCode.SEMICOLON, KeyCode.QUOTE,
        KeyCode.COMMA, KeyCode.PERIOD, KeyCode.SLASH -> true
        else -> false
    }
}

// KeyCode.isValidInput
fun KeyCode.isValidInput(): Boolean {
    return isDigitKey || isLetterKey || isSymbol()
}