package tableViewProblem.data

import javafx.beans.property.SimpleDoubleProperty
import javafx.beans.property.SimpleStringProperty

class TableData( text: String, num: Double) {
    val textVal = SimpleStringProperty( text )
    val numericVal = SimpleDoubleProperty( num )
}