package main.kotlin.tableViewProblem

import javafx.collections.ObservableList
import tableViewProblem.controls.exceltableview
import tableViewProblem.data.TableData
import tornadofx.*

class MainForm : View( "" ) {
    override val root = exceltableview( createList() ) {
        column( "текст", TableData::textVal ).makeEditable()
        column( "числа", TableData::numericVal ).makeEditable()
    }

    private fun createList() : ObservableList<TableData>
    {
        val list = observableListOf<TableData>()
        list.add( TableData( "", 0.0 ) )
        list.add( TableData( "row 2", 0.0 ) )
        list.add( TableData( "another row", 1.0 ) )
        return list
    }
}