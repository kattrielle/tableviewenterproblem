# Demonstration of bug with printing at table view

project build:
```bash
gradle wrapper
./gradlew run
```

bug: first symbol is missed while printing content to the TableView without pressing "enter" key
